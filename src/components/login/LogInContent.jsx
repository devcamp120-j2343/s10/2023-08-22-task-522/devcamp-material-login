import { Visibility, VisibilityOff } from "@mui/icons-material"
import { Box, Button, FormControl, IconButton, Input, InputAdornment, InputLabel, OutlinedInput, Typography } from "@mui/material"
import accLogin from '../../assets/images/accLogin.png'
import * as React from 'react';

const LogInContent = () => {

    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);
  
    const handleMouseDownPassword = (event) => {
      event.preventDefault();
    };

    return (
        <>
            <Box className="loginBox">
                <Typography variant="h2" className="loginTitle">Login</Typography>
                <FormControl sx={{ mt: '53px', width: '100%', borderBottom:'1px solid #ffffff' }} variant="standard">
                    <InputLabel htmlFor="username" className="loginItem">
                        Username
                    </InputLabel>
                    <Input
                        id="username"
                        className="loginItem"
                        endAdornment={
                            <InputAdornment position="end">
                                <img src={accLogin}/>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <FormControl sx={{ mt: '53px', width: '100%', borderBottom:'1px solid #ffffff' }} variant="standard">
                    <InputLabel htmlFor="password" className="loginItem">Password</InputLabel>
                    <Input
                    id="password"
                    className="loginItem"
                    type={showPassword ? 'text' : 'password'}
                    endAdornment={
                        <InputAdornment position="end">
                        <IconButton
                            sx={{color: '#FFF !important'}}
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                        >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                        </InputAdornment>
                    }
                    label="Password"
                    />
                </FormControl>
                <Button sx={{ mt: '53px', width: '100%' }} variant="contained" className="loginButton">Login</Button>
            </Box>
        </>
    )
}

export default LogInContent