import { Container, Grid } from "@mui/material"
import LogInContent from "../components/login/LogInContent"

const LogIn = () => {
    return (
        <>
            <Container maxWidth="xxl" className="bgLogin">
                <Grid container>
                    <Grid item sm={4} md={4} lg={4} xl={4} className="loginContainer">
                        <LogInContent/>
                    </Grid>
                </Grid>
            </Container>            
        </>
    )
}

export default LogIn